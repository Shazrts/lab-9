#include <stdio.h>
#include <string.h>

typedef float(*computeProduct)(struct Matrix *A, struct Matrix *B);

typedef void(*print)(struct Matrix *A);
typedef void(*initializeMatrix)(struct Matrix *A, int rows, int columns);
typedef void(*initializeVector)(struct Vector *A, int rows); 

typedef struct Matrix{
	int **array;
	int rows;
	int columns;
	computeProduct compute;
	initializeMatrix init;
	print printthis;
}MN_Matrix;


void printMatrix(struct Matrix *A){
	int i, j;
	for (i = 0; i < A->rows; i++)
	{
		for (j = 0; j < A->columns; j++)
			printf("%d\t", A->array[i][j]);
		printf("\n");
	}
}

float matrix_Prod(struct Matrix *A, struct Matrix *B)
{
	int c, d, k, sum = 0;
	int multiply[20][20];

	if (A->columns != B->rows)
		printf("Cannot Multiply because coloumn of A do not match rows of B \n");
	for (c = 0; c < A->rows; c++) {
		for (d = 0; d < B->columns; d++) {
			for (k = 0; k < B->rows; k++) {
				sum = sum + (A->array[c][k]) * (B->array[k][d]);
			}

			multiply[c][d] = sum;
			sum = 0;
		}
	}

	for (c = 0; c < A->rows; c++) {
		for (d = 0; d < B->columns; d++)
			printf("%d\t", multiply[c][d]);

		printf("\n");
	}
}


void initialMatrix(struct Matrix *A, int rows, int columns){
	int i;
	MN_Matrix temp;

	temp.rows = rows;
	temp.compute = matrix_Prod;
	temp.printthis = printMatrix;
	temp.columns = columns;
	temp.array = malloc(rows * sizeof(int *));

	for (i = 0; i < rows; i++)
	{
		temp.array[i] = malloc(columns * sizeof(int));
		if (temp.array[i] == NULL)
		{
			fprintf(stderr, "Out of memory\n");
			return;
		}
	}
	for (i = 0; i < 3; i++)
	{
		int j;
		for (j = 0; j < 3; j++)
			temp.array[i][j] = 1;
	}

	*A = temp;
}


typedef struct Vector{
	MN_Matrix vec;
	initializeVector init;
}N_Vector;


void initVector(struct Vector *A, int rows){
	N_Vector nvec;
	nvec.vec.init = initialMatrix;
	nvec.vec.init(&nvec.vec, rows, 1);
	*A = nvec;
}

int main() {
	MN_Matrix mat1;
	mat1.init = initialMatrix;
	mat1.init(&mat1, 3, 3);

	printf(" 1st Matrix \n");
	mat1.printthis(&mat1);

	MN_Matrix mat2;
	mat2.init = initialMatrix;
	mat2.init(&mat2, 3, 3);

	printf("\n 2nd Matrix: \n");
	mat2.printthis(&mat2);

	printf("\n Matrix Right Multiplication: \n");
	mat1.compute(&mat1, &mat2);

	N_Vector vect;
	vect.init = initVector;
	vect.init(&vect, 3);
	printf("Vector: \n");
	vect.vec.printthis(&vect.vec);

	return 0;
}
